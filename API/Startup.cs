using Domain.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(opt => opt.UseInMemoryDatabase("PrizeDB"));
            services.AddControllers();

            string firstNSimbolsOfText = Configuration.GetValue<string>("FirstNSimbolsOfText");
            services.AddSingleton(firstNSimbolsOfText);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "API ������ ��� ���������� ���������� � ���������� ������",
                    Version = "2022",
                    Description = "� - ������������. ������� �3."
                });
                /*var xfile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xpath = Path.Combine(AppContext.BaseDirectory, xfile);
                c.IncludeXmlComments(xpath);*/
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            app.UseHttpsRedirection();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "� - ������������. ������� �3.");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<DataContext>();
                SeedDataBase(context);
            }
        }
        private static void SeedDataBase(DataContext context)
        {
            var partisipants = new List<Participant>
            {
                new Participant
                { Id = 1,
                    Name = "Participant 1"
                },
                new Participant
                { Id = 2,
                    Name = "Participant 2"
                },
                new Participant
                {
                    Id = 3,
                    Name = "Participant 3"
                },
            };


            context.Participants.AddRange(partisipants);
            context.SaveChanges();
            var prizes = new List<Prize>
            {
                new Prize
                {
                     Id = 1,
                    Description = "Lorem ipsum dolor sit amet"
                },
                new Prize
                {
                     Id = 2,
                    Description = "consectetur adipiscing elit"
                },
               new Prize
                {
                    Id = 3,
                    Description = "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"
                },
            };

            context.Prizes.AddRange(prizes);
            context.SaveChanges();
            var promotions = new List<Promotion>
            {
                new Promotion
                {
                    Id= 1,
                    Name="promo 1",
                    Description="Description of promo 1",
                    Participants=partisipants,
                    Prizes=prizes
                },
                  new Promotion
                {
                    Id= 2,
                    Name="promo 2",
                    Description="Description of promo 2",
                    Participants=partisipants,
                    Prizes=prizes
                }
            };

            context.Promotions.AddRange(promotions);

            context.SaveChanges();

            var results = new List<Result>
            {
                new Result
                {
                    Winner = partisipants.First(),
                    Prize=prizes.Last(),
                },
                new Result
                {
                    Winner = partisipants.Last(),
                    Prize=prizes.First(),
                }
            };
            context.Results.AddRange(results);
            context.SaveChanges();
        }
    }
}
