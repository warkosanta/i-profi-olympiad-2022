﻿using Domain.DTOs;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PromoController : ControllerBase
    {
        private readonly DataContext _context;
        public PromoController( DataContext context)
        {
            _context = context;
        }

        // GET: api/<PrizeController>
        [HttpGet]
   
        public IEnumerable<ExtendedPromoDTO> Get()
        {
            return _context.Promotions.Select(p => new ExtendedPromoDTO
            {
                Description = p.Description,
                Name = p.Name,
                Id = p.Id
            });
        }

        // GET api/<PrizeController>/5
        [HttpGet("{id}")]
        public ActionResult<Promotion> Get(int id)
        {
            var promo = _context.Promotions.Find(id);
            if (promo == null) return NotFound();
            return _context.Promotions.Include(p => p.Participants)
                .Include(p => p.Prizes).First(p => p.Id == id);
        }

        [HttpPost]
        public ActionResult<int> Post(PromoDTO promo)
        {
            if (string.IsNullOrEmpty(promo.Name)) return BadRequest();

            var newPromotion = new Promotion
            {
                Name = promo.Name,
                Description = promo.Description,
            };

            _context.Promotions.Add(newPromotion);
            _context.SaveChanges();
            return newPromotion.Id;
        }

        // POST api/<PrizeController>
        [HttpPost]
        [Route("{id}/participant")]
        public ActionResult<int> Post(int id, ParticipantDTO participant)
        {
            if (string.IsNullOrEmpty(participant.Name)) return BadRequest();
            var promo = _context.Promotions.Find(id);
            if (promo == null) return NotFound();
            promo = _context.Promotions.Include(p => p.Participants)
                .Include(p => p.Prizes).First(p => p.Id == id);

            var newParticipant = new Participant
            {
                Name = participant.Name
            };

            _context.Participants.Add(newParticipant);
            _context.SaveChanges();


            promo.Participants.Add(newParticipant);
            _context.Update(promo);
            _context.SaveChanges();
            return newParticipant.Id;
        }

        [HttpPost]
        [Route("{id}/prize")]
        public ActionResult<int> Post(int id, PrizeDTO prize)
        {
            if (string.IsNullOrEmpty(prize.Description)) return BadRequest();
            var promo = _context.Promotions.Find(id);
            if (promo == null) return NotFound();
            promo = _context.Promotions.Include(p => p.Participants)
                .Include(p => p.Prizes).First(p => p.Id == id);
            var newPrize = new Prize
            {
                Description = prize.Description
            };

            _context.Prizes.Add(newPrize);
            _context.SaveChanges();


            promo.Prizes.Add(newPrize);
            _context.Update(promo);
            _context.SaveChanges();
            return newPrize.Id;
        }

        [HttpPost("{id}/raffle")]
        public ActionResult<List<RaffleDto>> Post(int id)
        {
            var promo = _context.Promotions.Find(id);
            if (promo == null) return NotFound();
            promo = _context.Promotions.Include(p => p.Participants)
                .Include(p => p.Prizes).First(p => p.Id == id);

            if (promo.Participants.Count != promo.Prizes.Count) return Conflict();

            return _context.Results
                .Where(r => promo.Participants
                .Contains(r.Winner)
                    && promo.Prizes.Contains(r.Prize))
                .Select(r => new RaffleDto { Prize = r.Prize, Winner = r.Winner }).ToList();
        }

        // PUT api/<PrizeController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] PromoDTO newPromo)
        {
            var oldPromo = _context.Promotions.Find(id);
            if (oldPromo == null) return NotFound();

            oldPromo.Description = newPromo.Description;
            oldPromo.Name = string.IsNullOrEmpty(newPromo.Name) ? oldPromo.Name : newPromo.Name;

            _context.Update(oldPromo);
            _context.SaveChanges();
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var oldPromo = _context.Promotions.Find(id);
            if (oldPromo == null) return NotFound();
            _context.Promotions.Remove(oldPromo);
            _context.SaveChanges();
            return Ok();
        }

        [HttpDelete("{promoId}/participant/{participantId}")]
        public ActionResult Delete(int promoId, int participantId)
        {
            var promo = _context.Promotions.Find(promoId);
            if (promo == null) return NotFound();
            promo = _context.Promotions.Include(p => p.Participants)
                .Include(p => p.Prizes).First(p => p.Id == promoId);
            var patricipant = _context.Participants.Find(participantId);
            if (patricipant == null) return NotFound();

            //var participants = oldPromo.Participants.Where(p => p.Id != participantId).ToList();

            promo.Participants.Remove(patricipant);
            _context.Update(promo);
            _context.SaveChanges();
            return Ok();
        }

        [HttpDelete("{promoId}/prize/{prizeId}")]
        public ActionResult DeletePrize(int promoId, int prizeId)
        {
            var promo = _context.Promotions.Find(promoId);
            if (promo == null) return NotFound();
            promo = _context.Promotions.Include(p => p.Participants)
                .Include(p => p.Prizes).First(p => p.Id == promoId);
            var prize = _context.Prizes.Find(prizeId);
            if (prize == null) return NotFound();

            promo.Prizes.Remove(prize);
            _context.Update(promo);
            _context.SaveChanges();
            return Ok();
        }
    }
}
